#!/usr/bin/python3

from babel.messages import pofile
from sys import argv


if ".po" not in argv[-1]:
    print("Please, pass a .po file as argument")
    exit(1)


f = pofile.read_po(open(argv[-1]))

n = 0
for msg in f:
    if msg.id:
        print("%s: %s" % (n, msg.id))
    n += 1

