# PO external translation toolkit

## A simple tool to translate your files via an external service

### How it works?


The toolkit reads the .po file and converts it into a
simple text file where every string is preceded by his line number.

Once your external service have fully translated the plain text file,
the toolkit reads the translated file and writes the strings back to the
.po file


### Getting started

1. Install Babel
`pip3 install Babel`

2. Copy your .po file in this repository
3. Run `./reader.py your-po.po > to-be-translated.txt`
4. Cut&paste the content of to-be-translated.txt into your external translator
5. Cut&paste the translation from your external translator in a .txt file
6. Run `./writer.py your-txt.tx your-po.po`
7. Replace old .po file with the your-po.po.new file 


### To-Do list

- Use argparser
