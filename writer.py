#!/usr/bin/python3

from babel.messages import pofile
from sys import argv


if ".txt" not in argv[-2]:
    print("Please, pass a .txt file as argument")
    exit(1)

if ".po" not in argv[-1]:
    print("Please, pass a .po file as argument")
    exit(1)


k = open(argv[-2])
_ = {}
for l in k.readlines():
    if l.endswith("\n"):
        l = l[:-1]
    _[int(l.split(":")[0])] = l.split(": ", 1)[1]

print(_)


f = pofile.read_po(open(argv[-1]))

n = 0
for msg in f:
    if msg.id:
        msg.string = _[n]
        print("%s: %s -> %s" % (n, msg.id, msg.string))
    n += 1

pofile.write_po(open(argv[-1]+".new", "wb"), f)
